package techies.vaap.saas.swipedowncustomlist;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

import it.sephiroth.android.library.picasso.Picasso;

public class CustomList extends ArrayAdapter<String>{

    private final Activity context;
    private final ArrayList<String> name;
    private final ArrayList<String> image_url;
    public CustomList(Activity context,
                      ArrayList<String> name, ArrayList<String> image_url) {
        super(context, R.layout.layout_single, name);
        this.context = context;
        this.name = name;
        this.image_url = image_url;

    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.layout_single, null, true);

        TextView names = (TextView) rowView.findViewById(R.id.name);
        ImageView imageView = (ImageView)rowView.findViewById(R.id.profilepic);
       // CircleImageView imageView = (CircleImageView) findViewById(R.id.image);
        Picasso.with(context).load(image_url.get(position)).transform(new CircleTransform()).into(imageView);
        names.setText(name.get(position));
        return rowView;
    }
}