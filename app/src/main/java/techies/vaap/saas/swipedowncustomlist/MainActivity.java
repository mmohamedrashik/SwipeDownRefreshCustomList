package techies.vaap.saas.swipedowncustomlist;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{
    private SwipeRefreshLayout swipeRefreshLayout;
    private ListView listView;
    ArrayList<String> name;
    ArrayList<String> image_url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listView);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.view);
        swipeRefreshLayout.setOnRefreshListener(this);
        name = new ArrayList<>();
        image_url = new ArrayList<>();
        name.add("Name 1");
        image_url.add("http://palmzlib.sourceforge.net/images/baboon.png");
        name.add("Name 2");
        image_url.add("http://ocw.mit.edu/faculty/michael-cuthbert/cuthbert.png");
        name.add("Name 3");
        image_url.add("http://www.firsttimesee.com/jjiang/data/profile.png");
        name.add("Name 4");
        image_url.add("http://www.omd.com/OMD/files/6f/6f079602-79f2-4156-b09a-29a6a0de4b76.png");
        name.add("Name 5");
        image_url.add("http://www.omd.com/OMD/files/6f/6f079602-79f2-4156-b09a-29a6a0de4b76.png");

    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        Toast.makeText(getApplicationContext(),"REFRESHED",Toast.LENGTH_LONG).show();
        CustomList adapter = new CustomList(MainActivity.this, name, image_url);
        listView.setAdapter(adapter);
    }
}
